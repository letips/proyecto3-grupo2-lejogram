import './Entrie.css';
import { useState } from 'react';
import { useToken } from '../../TokenContext';
import { Comments } from '../Comments/Comments';
import { Slider } from '../Slider/Slider';

// COMPONENTE ENTRIE:
// Partimos de una entrada (entrie)
const Entrie = ({ entrie, entries, setEntries }) => {
  console.log(`entrie`, entrie);
  // NO TOCAR ------------------------------------------------------------------
  const [token] = useToken();
  const [loading, setLoading] = useState();

  // Función que maneja el evento de like.
  const handleLikeEntrie = async (e, idEntrie, likedByMe) => {
    setLoading(true);
    console.log(likedByMe);
    e.target.classList.toggle('like');

    try {
      // Variable que almacenara el método a utilizar con el fetch.
      let method = likedByMe ? 'delete' : 'post';

      const res = await fetch(
        `http://localhost:4000/entries/${idEntrie}/likes`,
        {
          method,
          headers: {
            Authorization: token,
          },
        }
      );

      const body = await res.json();

      if (body.status === 'error') {
        alert(body.message);
      } else {
        setEntries(
          entries.map((entrie) => {
            if (entrie.id === idEntrie) {
              // Comprobamos si el tweet tiene la clase "like".
              const hasLikeClass = e.target.classList.contains('like');

              // Si la tiene incrementamos los likes de este tweet en +1, de lo contrario
              // decrementamos en -1.
              if (hasLikeClass) {
                entrie.likes++;
              } else {
                entrie.likes--;
              }

              // Invertimos el valor de likedByMe.
              entrie.likedByMe = !entrie.likedByMe;
            }

            // Retornamos el tweet.
            return entrie;
          })
        );
      }
    } catch (err) {
      console.error(err);
    } finally {
      setLoading(false);
    }
  };
  // NO TOCAR --------------------------------------------------------------------

  return (
    <li className="entrie">
      <header>
        <p>@{entrie.name || 'Pikachu'}</p>
        <time dateTime={entrie.created_at}>
          {new Date(entrie.created_at).toLocaleDateString('es-ES', {
            hour: '2-digit',
            minute: '2-digit',
            day: '2-digit',
            month: '2-digit',
            year: '2-digit',
          })}
        </time>
      </header>

      <div>
        <Slider entrie={entrie} key={entrie.id} />
        {/*         {entrie.photos && (
          <ul>
            {entrie.photos.map((photo) => {
              return (
                <li key={photo.id}>
                  <img
                    src={`http://localhost:4000/${photo.photo_name}`}
                    alt="foto"
                  ></img>
                </li>
              );
            })}
          </ul>
        )} */}
        <p>Place: {entrie.place}</p>
        <p>Description: {entrie.description}</p>
      </div>

      <Comments entrie={entrie} key={entrie.id} />

      <footer>
        <div>
          <div
            className={`heart ${token && entrie.likedByMe && 'like'}`}
            onClick={(e) => handleLikeEntrie(e, entrie.id, entrie.likedByMe)}
            disabled={loading}
          ></div>
          <p>{entrie.likes}</p>
        </div>

        {/*         {token && tweet.owner === 1 && (
          <button
            onClick={() => handleDeleteEntrie(tweet.id)}
            disabled={loading}
          >
            Eliminar
          </button>
        )} */}
      </footer>
    </li>
  );
};
export { Entrie };
