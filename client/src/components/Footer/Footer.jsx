import './Footer.css';

// COMPONENTE FOOTER:
const Footer = () => {
  return (
    <footer>
      <p>&copy; LeJoGram</p>
    </footer>
  );
};
export { Footer };
