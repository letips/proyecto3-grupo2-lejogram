import './EntrieSearch.css';
import { useEffect, useState } from 'react';
import { Entrie } from '../Entrie/Entrie';

// COMPONENTE ENTRYSEARCH:
const EntrieSearch = () => {
  const [keyword, setKeyword] = useState('');
  const [loading, setLoading] = useState(false);
  const [entries, setEntries] = useState(null);

  // Usamos useEffect para que haga un fetch a la DB nada más entrar a la página LeJoGram.
  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const res = await fetch('http://127.0.0.1:4000/entries');
        const body = await res.json();
        console.log(`body`, body);

        if (body.status === 'error') {
          alert(body.message);
        } else {
          setEntries(body.data);
        }
      } catch (err) {
        console.error(err);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  // Función para manejar el envio del formulario de buscar entradas.
  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      const res = await fetch(
        `http://localhost:4000/entries?keyword=${keyword}`
      );
      const body = await res.json();

      if (body.status === 'error') {
        alert(body.message);
      } else {
        setEntries(body.data);
      }
      /*       setKeyword(''); */
    } catch (err) {
      console.error(err);
    } finally {
      setLoading(false);
    }
  };

  return (
    <main className="entrie-search">
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          value={keyword}
          onChange={(e) => setKeyword(e.target.value)}
        />
        <button disabled={loading}>Buscar</button>
      </form>

      <ul className="entrie-list">
        {entries &&
          entries.map((entrie) => {
            return (
              <Entrie
                entrie={entrie}
                entries={entries}
                setEntries={setEntries}
                key={entrie.id}
              />
            );
          })}
      </ul>
    </main>
  );
};
export { EntrieSearch };
