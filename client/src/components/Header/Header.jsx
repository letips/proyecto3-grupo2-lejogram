import './Header.css';
import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { useToken } from '../../TokenContext';

// COMPONENTE HEADER:
const Header = () => {
  const [token, setToken] = useToken();
  const [user, setUser] = useState(null);

  // Utilizamos useEffect para acceder a los datos del usuario si existe token.
  useEffect(() => {
    const userData = async () => {
      try {
        const res = await fetch('http://localhost:4000/users', {
          headers: {
            Authorization: token,
          },
        });

        const body = await res.json();

        if (body.status === 'error') {
          alert(body.message);
        } else {
          setUser(body.data.user);
        }
      } catch (err) {
        console.error(err);
      }
    };

    // Si el usuario está logeado buscamos sus datos.
    if (token) userData();
  }, [token]);

  return (
    <header>
      <>
        <NavLink to="/">
          {<img src="./logo-lejogram.png" alt="logo lejogram" />}
        </NavLink>
      </>
      <nav>
        {token && user && <p>@{user.name}</p>}
        <div className="navlink-container">
          {!token && (
            <div className="button">
              <NavLink to="/login">Login</NavLink>
            </div>
          )}
          {!token && (
            <div className="button">
              <NavLink to="/register">Registro</NavLink>
            </div>
          )}
          {token && (
            <div className="button">
              <NavLink to="/publish">Publicar</NavLink>
            </div>
          )}
          {token && (
            <div className="button" onClick={() => setToken(null)}>
              <p>Cerrar Sesión</p>
            </div>
          )}
        </div>
      </nav>
    </header>
  );
};
export { Header };
