import './Comments.css';
import { useState } from 'react';
import { useEffect } from 'react';
import { useToken } from '../../TokenContext';

const Comments = ({ entrie }) => {
  const [token] = useToken();

  const [inputText, setInputText] = useState('');

  const [loading, setLoading] = useState(false);
  const [comments, setComments] = useState([]);
  console.log(`ArraydeComentarios`, comments);

  // Usamos useEffect para que haga un fetch a la DB y obtenga los comentarios.
  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await fetch('http://127.0.0.1:4000/comments');
        const body = await res.json();
        /*         console.log(`comentariosbyDB`, body); */

        if (body.status === 'error') {
          alert(body.message);
        } else {
          setComments(body.data);
        }
      } catch (err) {
        console.error(err);
      }
    };

    fetchData();
  }, []);

  // Función que maneja el envío del formulario de registro.
  const handleSubmit = async (
    token,
    inputText,
    setInputText,
    setLoading,
    id
  ) => {
    setLoading(true);

    const pruebita = {
      method: 'post',
      headers: {
        Authorization: token,
      },
      body: JSON.stringify({
        comment: inputText,
      }),
    };
    console.log('pruebita', pruebita);

    try {
      const res = await fetch(
        `http://localhost:4000/entries/${id}/comment`,
        pruebita
      );

      const body = await res.json();

      if (body.status === 'error') {
        alert(body.message);
      } else {
        setComments(body);
      }
    } catch (err) {
      console.error(err);
    } finally {
      setLoading(false);
      setInputText('');
    }
  };

  return (
    <div className="comments-container">
      <h4>Comentarios de esta entrada:</h4>

      <div className="all-comments-container">
        {comments.map((comment) => {
          if (entrie.id === comment.entry_id) {
            return (
              <>
                <time dateTime={comment.created_at}>
                  {new Date(comment.created_at).toLocaleDateString('es-ES', {
                    hour: '2-digit',
                    minute: '2-digit',
                    day: '2-digit',
                    month: '2-digit',
                    year: '2-digit',
                  })}
                </time>
                <ul className="ul-comments">
                  <li key={comment.id}>
                    <p>{`@${comment.name} posted: ${comment.comment}`}</p>
                  </li>
                </ul>
              </>
            );
          }
        })}
      </div>

      <div
        className={
          token
            ? 'comment-form-container-visible'
            : 'comment-form-container-hidden'
        }
      >
        <form
          onSubmit={(e) => {
            e.preventDefault();
            handleSubmit(token, inputText, setInputText, setLoading, entrie.id);
          }}
        >
          <input
            type="text"
            value={inputText}
            placeholder="Escribe tu comentario"
            onChange={(e) => setInputText(e.target.value)}
          />
          <button disabled={loading}>Post</button>
        </form>
      </div>
    </div>
  );
};

export { Comments };
