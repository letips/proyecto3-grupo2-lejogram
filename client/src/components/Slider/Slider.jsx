import { useState } from 'react';
import { useEffect } from 'react';

// Importamos los estilos de Slider.module.css
import estilos from './Slider.module.css';

const Slider = ({ entrie }) => {
  const entryImages = entrie.photos;
  const imagenes = entryImages.map((photo) => {
    return `http://localhost:4000/${photo.photo_name}`;
  });
  /*   console.log(`imagenes`, imagenes); */

  // Creamos un estado para la imagen actual. La imagen actual va a ser la que esté en primera posicíón (posición cero) del array de imágenes.
  const [imagenActual, setImagenActual] = useState(0);

  // Creamos una variable para saber cuántas imágenes hay en el array de imágenes
  const cantidad = imagenes?.length;

  // Return prematuro para evitar errores: Si el array de imágenes no es un array y si la cantidad de imágenes que contiene el array es cero; return.
  if (!Array.isArray(imagenes) || cantidad === 0) {
    return;
  }

  // FUNCIÓN MANEJADORA DE BOTÓN ANTERIOR:
  const handleBack = () => {
    setImagenActual(imagenActual === 0 ? cantidad - 1 : imagenActual - 1);
  };

  // FUNCIÓN MANEJADORA DE BOTÓN SIGUIENTE:
  const handleNext = () => {
    setImagenActual(imagenActual === cantidad - 1 ? 0 : imagenActual + 1);
  };

  // Div que contiene botón de anterior + mapeado del array de imágenes que retorna una imágen en cada vuelta + botón de siguiente.
  // Dentro del div del map: Si la imagen actual del mapeado corresponde al index, muestra la imagen.
  return (
    <div className={estilos.container1}>
      <div className={estilos.container2}>
        {imagenes.length > 1 ? (
          <button className={estilos.button} onClick={handleBack}>
            ←
          </button>
        ) : null}

        {imagenes.map((imagen, index) => {
          return (
            <div
              className={
                imagenActual === index
                  ? `${estilos.slide} ${estilos.active}`
                  : estilos.slide
              }
            >
              {imagenActual === index && (
                <img key={index} src={imagen} alt="imagen" />
              )}
            </div>
          );
        })}

        {imagenes.length > 1 ? (
          <button className={estilos.button} onClick={handleNext}>
            →
          </button>
        ) : null}
      </div>
    </div>
  );
};

export { Slider };
