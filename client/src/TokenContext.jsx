import { createContext, useState, useContext } from 'react';

const TokenContext = createContext(null);

//
const TokenProvider = ({ children }) => {
  const [token, setToken] = useState(localStorage.getItem('token'));

  const setTokenInLocalStorage = (newToken) => {
    if (!newToken) {
      localStorage.removeItem('token');
    } else {
      localStorage.setItem('token', newToken);
    }

    setToken(newToken);
  };

  return (
    <TokenContext.Provider value={[token, setTokenInLocalStorage]}>
      {children}
    </TokenContext.Provider>
  );
};

//
const useToken = () => {
  return useContext(TokenContext);
};

export { TokenProvider, useToken };
