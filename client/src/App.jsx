// Importamos css
import './App.css';

// Importamos Routes y Route
import { Route, Routes } from 'react-router-dom';

// Importamos componentes:
import { Header } from './components/Header/Header';
import { Footer } from './components/Footer/Footer';
import { EntrieSearch } from './components/EntrieSearch/EntrieSearch';
import { Register } from './components/Register/Register';
import { Login } from './components/Login/Login';
import { CreateEntrie } from './components/CreateEntrie/CreateEntrie';

function App() {
  return (
    <div className="app">
      <Header />

      <Routes>
        <Route path="/" element={<EntrieSearch />} />
        <Route path="/register" element={<Register />} />
        <Route path="/login" element={<Login />} />
        <Route path="/publish" element={<CreateEntrie />} />
        <Route path="*" element={<EntrieSearch />} />
      </Routes>

      <Footer />
    </div>
  );
}

export default App;
