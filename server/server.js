// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% INIT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
`use strict`;

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DEPENDENCIES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

require('dotenv').config();

const { PORT } = process.env;

const express = require('express');
const morgan = require(`morgan`);
const cors = require('cors');
const path = require('path');
const fileUpload = require('express-fileupload');

const app = express();

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% MIDDLEWARES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

app.use(cors());
app.use(express.static('uploads'));

app.use(morgan('dev'));

app.use(express.json());

app.use(fileUpload());

// MIDDLEWARE isAuth - para verificar que un usuario está logueado (Authorization Token):
const isAuth = require('./MIDDLEWARES/isAuth');

// MIDDLEWARE isAuthOptional - para verificar que un usuario es el creador de una entrada:
const isAuthOptional = require('./MIDDLEWARES/isAuthOptional');

// MIDDLEWARE entryExists - para verificar si una entrada (path param con id en la petición) existe en la DB:
const entryExists = require('./MIDDLEWARES/entryExists');

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CONTROLLERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/**#######################
 * ## USERS CONTROLLERS ##
 * #######################
 */
const {
  createUserController,
  loginUserController,
  getUserController,
  getOwnUserController,
} = require('./CONTROLLERS/users/index');

/**#########################
 * ## ENTRIES CONTROLLERS ##
 * #########################
 */
const {
  newEntryController,
  listEntriesController,
  getEntryByIdController,
  doLikeController,
  deleteLikeController,
  createCommentController,
  listCommentsController,
} = require('./CONTROLLERS/entries/index');

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ENDPOINTS (PETICIONES) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/**#####################
 * ## USERS ENDPOINTS ##
 * #####################
 */
//--------------------------------------------------------------------------
// - 1. POST - [/users] - Registro | Crear un usuario.
app.post('/users', createUserController);
//      - createUserController requiere la FUNCIÓN QUERY createUserInDb.js de (DB/queries/users).
//--------------------------------------------------------------------------
// - 2. POST - [/users/login] - Login | Hará el login de un usuario y devolverá el TOKEN.
app.post('/users/login', loginUserController);
//      - loginUserController requiere la FUNCIÓN QUERY selectUserByEmailInDb.js de (DB/queries/users).
//--------------------------------------------------------------------------
// - 3. GET [/users/:id] - Ver el perfil de un usuario con su galería de fotos [Si eres el propio usuario del pefirl mostrar más datos (por ejemplo el email).]
app.get('/users/:id', isAuthOptional, getUserController);
//      - getUserController pasa por el middleware isAuthOptional.
//      - getUserController requiere, finalmente, la FUNCIÓN QUERY selectUserByIdQuery.js de (DB/queries/users).
//--------------------------------------------------------------------------
// - EXTRA PARA REACT - GET [/users]
app.get('/users', isAuthOptional, getOwnUserController);
//--------------------------------------------------------------------------
// - 4. PATCH [/users/:id] - Gestión del perfil (cambios en los datos de registro) | Solo el propio usuario | [Token-obligatorio] | [OPCIONAL].
//      - FIX ME: PENDIENTE DE IMPLEMENTACIÓN.
//--------------------------------------------------------------------------

/**#######################
 * ## ENTRIES ENDPOINTS ##
 * #######################
 */
//--------------------------------------------------------------------------
// - 1. POST [/entries] - Hacer una publicación con un máximo de 3 fotos y obligatorio que al menos haya una foto (la foto debe ajustarse automáticamente a un tamaño máximo y unas proporciones establecidas por la plataforma). Y añadirle una descripción de forma opcional. | [Token-obligatorio].
app.post('/entries', isAuth, newEntryController);
//      - newEntryController pasa por el middleware isAuth.
//      - newEntryController requiere, finalmente, la FUNCIÓN QUERY createNewEntryAndPhotosInDbQuery.js de (DB/queries/entries).
//--------------------------------------------------------------------------
// - 2. GET [/entries] - Ver las últimas entradas publicadas por otros usuarios de la más reciente a la más antigua.
app.get('/entries', isAuthOptional, listEntriesController);
//      - listEntriesController requiere la FUNCIÓN QUERY selectAllEntriesQuery.js de (DB/queries/entries).
//--------------------------------------------------------------------------
// - 3. GET [/entries/:id] - Buscar entradas por id. Solo funcional para usuarios registrados | [Token-obligatorio].
app.get('/entries/:id', isAuth, entryExists, getEntryByIdController);
//      - getEntryByIdController pasa por el middleware isAuth y luego por el middleware entryExists.
//      - getEntryByIdController requiere, finalmente, la FUNCIÓN QUERY getEntrieAndPhotosFromDbQuery.js de (DB/queries/entries).
//--------------------------------------------------------------------------
// - 4. POST - [/entries/:id/likes] - Hacer un "like" a una entrada | [Token-obligatorio].
app.post('/entries/:id/likes', isAuth, entryExists, doLikeController);
//      - doLikeController pasa por el middleware isAuth y luego por el middleware entryExists.
//      - doLikeController requiere, finalmente, la FUNCIÓN QUERY createLikeInDbQuery.js de (DB/queries/entries).
//--------------------------------------------------------------------------
// - 5. DELETE - [/entries/:id/likes] - Eliminar un "like" de una entrada | [Token-obligatorio].
app.delete('/entries/:id/likes', isAuth, entryExists, deleteLikeController);
//      - deleteLikeController pasa por el middleware isAuth y luego por el middleware entryExists.
//      - deleteLikeController requiere, finalmente, la FUNCIÓN QUERY deleteLikeInDbQuery.js de (DB/queries/entries).
//--------------------------------------------------------------------------
// - 6. POST [/entries/:id/comment] - Comentar una entrada (no se permiten comentarios a comentarios) | [Token-obligatorio] | [OPCIONAL].
app.post('/entries/:id/comment', isAuth, entryExists, createCommentController);
//      - createCommentController pasa por el middleware isAuth y luego por el middleware entryExists.
//      - createCommentController requiere, finalmente, la FUNCIÓN QUERY deleteLikeInDbQuery.js de (DB/queries/entries).
//--------------------------------------------------------------------------
// - EXTRA PARA REACT - GET [/comments]
app.get('/comments', isAuthOptional, listCommentsController);
//--------------------------------------------------------------------------

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ERROR MIDDLEWARES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/**##################################
 * ## CONTROLLED ERRORS MIDDLEWARE ##
 * ##################################
 */
app.use((error, req, res, next) => {
  console.error(error);
  res.status(error.statusCode || 500).send({
    status: 'error',
    message: error.message,
  });
});

/**######################################
 * ## ROUTE NOT FOUND ERROR MIDDLEWARE ##
 * ######################################
 */
app.use((req, res) => {
  res.status(404).send({
    status: 'error',
    message: 'Not found',
  });
});

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SERVER ACTIVATION MIDDLEWARE %%%%%%%%%%%%%%%%%%%%%%%%%%

app.listen(PORT, () => {
  console.log(`Servidor funcionando en http://127.0.0.1:${PORT}`);
});
