// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% IMPORTACIONES NECESARIAS %%%%%%%%%%%%%%%%%%%%%%%%%%

const createCommentInDbQuery = require('../../DB/queries/entries/createCommentInDbQuery');

const { generateError } = require('../../helpers');

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FUNCIÓN CONTROLADORA createCommentController %%%%%%%

// Función controladora para crear un like a una entrada de la DB:
const createCommentController = async (req, res, next) => {
  try {
    console.log(`AQUIIIIIIIIIIIIIIII`, req.params);
    console.log(`ESTOOOOOOOOOOOOY`, req.user.id);
    console.log(`LOCOOOOOOOOOOOOOO`, req.body);

    const { id } = req.params;

    const userId = req.user.id;

    const { comment } = req.body;

    // FIX ME: Esto se debería de validar con JOI.
    if (!comment) {
      throw generateError(
        'Debes enviar un comentario de hasta 250 caracteres.',
        400
      );
    }

    // Llamamos a la FUNCIÓN QUERY createCommentInDbQuery de (DB/queries/entries):
    await createCommentInDbQuery(id, userId, comment);

    res.status(200).send({
      status: 'ok',
      message: 'Comentario a la entrada correcto.',
    });
  } catch (error) {
    next(error);
  }
};

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% EXPORTAMOS createCommentController %%%%%%%%%%%%%%%%%

module.exports = createCommentController;
