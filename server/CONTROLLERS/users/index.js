// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% IMPORTACIONES NECESARIAS %%%%%%%%%%%%%%%%%%%%%%%%%%

const createUserController = require('./createUserController');
const loginUserController = require('./loginUserController');
const getUserController = require('./getUserController');
const getOwnUserController = require('./getOwnUserController');

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% EXPORTAMOS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

module.exports = {
  createUserController,
  loginUserController,
  getUserController,
  getOwnUserController,
};
