// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% IMPORTACIONES NECESARIAS %%%%%%%%%%%%%%%%%%%%%%%%%%

const { generateError } = require('../../helpers');

const createUserInDb = require('../../DB/queries/users/createUserInDb');

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FUNCIÓN CONTROLADORA createUserController %%%%%%%%%%%

// Función controladora para crear un usuario en la DB.

const createUserController = async (req, res, next) => {
  try {
    const { name, email, password } = req.body;

    if (!email || !password) {
      throw generateError('Debes enviar un email y una password', 400);
    }

    // Llamamos a la FUNCIÓN QUERY createUserInDv de (DB/queries/users)
    await createUserInDb(name, email, password);

    res.send({
      status: 'ok',
      message: 'Usuario creado',
    });
  } catch (error) {
    next(error);
  }
};

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% EXPORTAMOS createUserController %%%%%%%%%%%%%%%%%%%%%%

module.exports = createUserController;
