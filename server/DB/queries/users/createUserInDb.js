// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% IMPORTACIONES NECESARIAS %%%%%%%%%%%%%%%%%%%%%%%%%%

const bcrypt = require('bcrypt');

const { generateError } = require('../../../helpers');

const { getDB } = require('../../getDbConnection');

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FUNCIÓN QUERY createUserInDb %%%%%%%%%%%%%%%%%%%%%%

// Función que crea un usuario en la DB y devuelve su id:

const createUserInDb = async (name, email, password) => {
  let connection;

  try {
    connection = await getDB();

    const [users] = await connection.query(
      `SELECT id FROM users WHERE email = ?`,
      [email]
    );

    if (users.length > 0) {
      generateError('Ya existe un usuario con ese email', 409);
    }

    const hashedPass = await bcrypt.hash(password, 10);

    await connection.query(
      `INSERT INTO users (name, email, password) VALUES (?, ?, ?)`,
      [name, email, hashedPass]
    );
  } finally {
    if (connection) connection.release();
  }
};

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% EXPORTAMOS createUserInDb %%%%%%%%%%%%%%%%%%%%%%%%%%

module.exports = createUserInDb;
