// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% IMPORTACIONES NECESARIAS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

const { getDB } = require('../../getDbConnection');

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FUNCIÓN QUERY selectAllEntriesQuery %%%%%%%%%%%%%%%%%%

// Función que se encarga de listar (en base a una keyword de búsqueda) todas las entradas, con sus respectivas fotos, en orden descendente de creación.
// El id de después de keyword es el id de usuario.
const selectAllEntriesQuery = async (keyword = '', id) => {
  let connection;

  try {
    connection = await getDB();

    const [entries] = await connection.query(
      `
        SELECT e.id, e.created_at, e.place, e.description, e.user_id, u.email, u.name,
        COUNT(l.id) AS likes, BIT_OR (l.user_id=?) AS likedByMe
        FROM entries AS e
        LEFT JOIN users AS u ON (u.id = user_id)
        LEFT JOIN likes AS l ON (l.user_id = u.id)
        WHERE e.description LIKE ?
        GROUP BY e.id
        ORDER BY e.created_at DESC;
      `,
      [id, `%${keyword}%`]
    );

    let entriesWithPhotos = [];

    // Verificamos si tengo entries. Si entries.length > 0 siginifica que, al menos, hay una entry.
    // Mapeamos las entries y saco los id de cada una y los almaceno en la variable "arrayIDs":
    if (entries.length > 0) {
      const [photos] = await connection.query(
        `
          SELECT *
          FROM photos;
        `
      );

      // Juntamos la entries con las photos en un array llamado "photoEntry":
      entriesWithPhotos = entries.map((entry) => {
        const photoEntry = photos.filter((photo) => {
          return photo.entry_id === entry.id;
        });
        return {
          ...entry,
          photos: photoEntry,
        };
      });

      return entriesWithPhotos;
    }
  } finally {
    if (connection) connection.release();
  }
};

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% EXPORTAMOS selectAllEntriesQuery %%%%%%%%%%%%%%%%%%%%%%

module.exports = selectAllEntriesQuery;
